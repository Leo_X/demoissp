# Tools

[![CI Status](https://img.shields.io/travis/1099879973@qq.com/Tools.svg?style=flat)](https://travis-ci.org/1099879973@qq.com/Tools)
[![Version](https://img.shields.io/cocoapods/v/Tools.svg?style=flat)](https://cocoapods.org/pods/Tools)
[![License](https://img.shields.io/cocoapods/l/Tools.svg?style=flat)](https://cocoapods.org/pods/Tools)
[![Platform](https://img.shields.io/cocoapods/p/Tools.svg?style=flat)](https://cocoapods.org/pods/Tools)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Tools is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Tools'
```

## Author

1099879973@qq.com, 1099879973@qq.com

## License

Tools is available under the MIT license. See the LICENSE file for more info.
