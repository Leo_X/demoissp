#
# Be sure to run `pod lib lint Tools.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |spec|


  spec.name         = "Tools"
  spec.version      = "1.0.6"
  spec.summary      = "工具组件"
  spec.description  = "a lib for Tools"
  spec.homepage     = "https://x-token-auth@bitbucket.org/Leo_X/demoissp"
  spec.license      = "LICENSE"
  spec.author             = { "SunnyXM" => "1099879973@qq.com" }
  spec.platform     = :ios, "9.0"
  spec.source       = { :git => "https://x-token-auth@bitbucket.org/Leo_X/demoissp.git", :tag => "#{spec.version}" }

  spec.source_files  = "Classes/**/*.{h,m}"
  spec.exclude_files = "Classes/Exclude"
  # spec.resources = "Tools/**/*.png"
  spec.framework  = "UIKit"
  spec.framework  = "Foundation"
  spec.requires_arc = true
  # spec.dependency 'AFNetworking', '~> 3.2.1'
  # spec.dependency 'Masonry', '~> 1.1.0'
  # spec.dependency 'YYKit', '~> 1.0.9'
  # spec.dependency 'SDWebImage', '~> 5.0.3'
  
end